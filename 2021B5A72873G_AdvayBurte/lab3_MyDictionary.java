import java.util.*;
class MyDictionary {
  // Main Driver Method (optional in our modular design)
  public static void main(String args[]) {
    System.out.println("The best way to use this type is to run another app main and instantiate a dictionary in it.");
  }
  private Record[] arr;
  private int size=0;
  private int length=0;
  public int Size() { return length; }
  public int MaxSize() { return size; }
  MyDictionary(int initialSize) {
    arr=new int[initialSize];
    size=initialSize;
    length=0;
  }
  public void Insert(int elem) {
    if(size>length) {
      arr[length++];
    }
    else System.out.println("Size" + size + " not enough for holding an extra element after " + length + " length");
  // public void Sort(int[] arr){
    

  // }

  }
  public void Show() {
    System.out.print("Printing Dictionary of " + size + " Size and " + length + " Occupancy:");
    for (int i = 0; i < length; i++) {
      System.out.print(" " + arr[i]);
    }
    for (int i = length; i < size; i++) {
      System.out.print(" .");
    }
    System.out.println("");
  }
}

class Record {
  private int key;
  private double value;

  public Record(double value){
    this.setKeyValue(value);
  }

  public int getKey() {
    return this.key;
  }

  public double getValue() {
    return this.value;
  }

  private void setKeyValue(double value) {
    this.value = value;
    if(value>=0){
      if(value - Math.floor(value) < 0.5){
        this.key = (int) Math.floor(value);
      }
      else{
        this.key = (int) Math.ceil(value);
      }
    }
    else{
      if(Math.abs(value) - Math.abs(Math.floor(value)) < 0.5){
        this.key = (int) Math.ceil(value);
      }
      else{
        this.key = (int) Math.floor(value);
      }
    }
  }

}
